import React, { Component } from "react";

class Like extends Component {
  render() {
    let selectedIcon = "fa fa-thumbs-up";
    if (!this.props.liked) selectedIcon = "fa fa-thumbs-o-up";
    return (
      <i
        onClick={this.props.onClick}
        style={{ cursor: "pointer" }}
        className={selectedIcon}
        aria-hidden="true"
      />
    );
  }
}

export default Like;
