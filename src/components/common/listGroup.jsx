import React from "react";
import { ListGroup } from "react-bootstrap";

const ListGroups = props => {
  const { items } = props;
  return (
    <ListGroup>
      {items.map(item => (
        <ListGroup.Item key={item._id}>{item.name}</ListGroup.Item>
      ))}
    </ListGroup>
  );
};

export default ListGroups;
